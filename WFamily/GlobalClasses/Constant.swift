//
//  Constant.swift
//  Whats Web
//
//  Created by Sameer's MacBook Pro on 10/06/19.
//  Copyright © 2019 Sameer's MacBook Pro. All rights reserved.
//

import UIKit

//let kBaseUrl = "http://52.51.54.41:5000/"   //Old Url
let kBaseUrl = "http://52.206.49.241/appentus/index.php/Api/" //New Url
let kCreateUser = kBaseUrl + "create_user"
let kSubscribeUser = kBaseUrl + "subscribeUser"
let kGetSubscriber = kBaseUrl + "getSubscriberUser"
let kSuccessPayment = kBaseUrl + "successPayment"
let kGetReports = kBaseUrl + "getReports"
let kGetPlans = kBaseUrl + "getPlans"
let kDelete = kBaseUrl + "clearReport"
let kUpdateTracking = kBaseUrl + "updateTracking"
let kChangeTracking = kBaseUrl + "changeTrackingStatus"

let kHeader = ["password" : "123456"]

let KAppStoreID = ""
let kNumberTrack = "numberTrack"
let kTrackDict = "trackDict"
let kCurrentTrackDateTime = "currentTrackDateTime"
let kTotalSeconds = "totalSecond"
let kTrialExpired = "trialExpired"
let kPremiumTrack = "PremiumTrack"

var fcmTokenStr: String?

let cornerRadius = CGFloat(15.0)
let shadowOpacity = Float(0.5)
let shadowRadius = CGFloat(5)

let usedFontName = "Helvetica"

enum iPhoneDeviceEnum : String {
    case iPhone_4_4s = "4_4s"
    case iPhone_5_5s_5c_SE = "5_5s_5c_SE"
    case iPhone_6_6s_7_8 = "6_6s_7_8"
    case iPhone_XR = "iPhone_XR"
    case iPhone_6Plus_6sPlus_7Plus_8Plus = "6Plus_6sPlus_7Plus_8Plus"
    case iPhone_X_XS = "X_XS"
    case iPhone_XSMax = "XSMax"
}
