//
//  Extension.swift
//  Whats Web
//
//  Created by Sameer's MacBook Pro on 07/06/19.
//  Copyright © 2019 Sameer's MacBook Pro. All rights reserved.
//

import UIKit
//import Alamofire

//TO check  internet connection in device
class Connectivity {
    class func isConnectedToInternet() ->Bool {
        //return NetworkReachabilityManager()!.isReachable
        return true
    }
}

extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

extension UIViewController {
    
    func rateWhatsWeb() {
        
        guard let appStoreUrl = URL.init(string: "itms-apps://itunes.apple.com/app/" + KAppStoreID)  else {
            print("Invalid Url!")
            return
        }
        
        if UIApplication.shared.canOpenURL(appStoreUrl) {
            UIApplication.shared.open(appStoreUrl, options: [:], completionHandler: nil)
        }
    }
    
    func shareWhatsWeb() {
        
        let firstActivityItem = "Look at this awesome app Whats Web !"
        let secondActivityItem : NSURL = NSURL(string: "https://itunes.apple.com/us/app/WhatScan-QR-Scanner-DirectChat/id\(KAppStoreID)?ls=1&mt=8")!
        
        // If you want to put an image
        //let image : UIImage = UIImage(named: "image.jpg")!
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [firstActivityItem, secondActivityItem], applicationActivities: nil)
        
        // This lines is for the popover you need to show in iPad
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        // This line remove the arrow of the popover to show in iPad
        //activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.allZeros
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToWeibo,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.postToFlickr,
            UIActivity.ActivityType.postToVimeo,
            UIActivity.ActivityType.postToTencentWeibo,
        ]
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view?.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func showaAlert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlert(_ title: String, message: String, alertButtonTitles: [String], alertButtonStyles: [UIAlertAction.Style], vc: UIViewController, completion: @escaping (Int)->Void) -> Void
    {
        let alert = UIAlertController(title: title,message: message,preferredStyle: UIAlertController.Style.alert)
        
        for title in alertButtonTitles {
            let actionObj = UIAlertAction(title: title,style: alertButtonStyles[alertButtonTitles.firstIndex(of: title)!], handler: { action in
                completion(alertButtonTitles.firstIndex(of: action.title!)!)
            })
            
            alert.addAction(actionObj)
        }
        
        //alert.view.tintColor = Utility.themeColor
        
        //vc will be the view controller on which you will present your alert as you cannot use self because this method is static.
        vc.present(alert, animated: true, completion: nil)
    }
   
    func showToast(message : String, seconds: Double) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.backgroundColor = UIColor.black
        alert.view.alpha = 0.6
        alert.view.layer.cornerRadius = 15
        
        /*
        self.present(alert, animated: true) {
            alert.view.superview?.isUserInteractionEnabled = true
         alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(self.tapOutsideAlert)))
        }
        */
        
        self.present(alert, animated: true) {
            DispatchQueue.main.asyncAfter(deadline: .now() + seconds, execute: {
                self.dismiss(animated: true, completion: nil)
            })
        }
        
    }
    
    @objc func tapOutsideAlert() {
        self.dismiss(animated: true, completion: nil)
    }
    
}

