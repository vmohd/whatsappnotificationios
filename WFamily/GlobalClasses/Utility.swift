//
//  Utility.swift
//  Whats Web
//
//  Created by Sameer's MacBook Pro on 06/06/19.
//  Copyright © 2019 Sameer's MacBook Pro. All rights reserved.
//

import UIKit

class Utility: NSObject {
    
    static func styleTextField(_ textfield:UITextField) {
        
        // Create the bottom line
        let bottomLine = CALayer()
        
        bottomLine.frame = CGRect(x: 0, y: textfield.frame.height - 2, width: textfield.frame.width, height: 2)
        
        bottomLine.backgroundColor = UIColor.init(red: 48/255, green: 173/255, blue: 99/255, alpha: 1).cgColor
        
        // Remove border on text field
        textfield.borderStyle = .none
        
        // Add the line to the text field
        textfield.layer.addSublayer(bottomLine)
        
    }
    
    //MARK: Check App Install or not
    static func schemeAvailable(scheme: String) -> Bool {
        if let url = URL(string: scheme) {
            return UIApplication.shared.canOpenURL(url)
        }
        return false
    }
        
    //Load vc from Main storyboard
    class func loadViewControllerFromMainStoryBoard(identifier: String) -> Any {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: identifier)
        return controller
    }
    
    class func loadViewControllerFromSettingStoryBoard(identifier: String) -> Any {
        let storyBoard = UIStoryboard(name: "Setting", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: identifier)
        return controller
    }
    
    class func getIphoneDevice(vc:UIViewController) -> String {
        
        switch UIScreen.main.nativeBounds.height {
        case 960:
            print("iPhones_4_4S")
            return "4_4s"
        case 1136:
            print("iPhones_5_5s_5c_SE")
            return "5_5s_5c_SE"
        case 1334:
            print("iPhones_6_6s_7_8")
            return "6_6s_7_8"
        case 1792:
            print("iPhone_XR")
            return "iPhone_XR"
        case 1920, 2208:
            print("iPhones_6Plus_6sPlus_7Plus_8Plus")
            return "6Plus_6sPlus_7Plus_8Plus"
        case 2436:
            print("iPhones_X_XS")
            return "X_XS"
        case 2688:
            print("iPhone_XSMax")
            return "XSMax"
        default:
            print("iPhone")
            return "iPhone"
        }
    }
    
    class func setCurrentTime_Date() -> String {
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //formatter.timeZone = TimeZone.local
        formatter.locale = Locale(identifier: "fr_FR")
        //formatter.timeZone = TimeZone.init(abbreviation: "UTC")
        
        let myString = formatter.string(from: Date()) // string purpose I add here
        // convert your string to date
        
        return myString
        /*
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MMM-yyyy"
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        print(myStringafd)
        */
    }
   
//    class func getCurrentDate() -> String {
//        if let ctt = UserDefaults.standard.string(forKey: kCurrentTrackDateTime) {
//            let str = String.init(decoding: ctt, as: UTF8.self)
//            return str
//        }
//    }
}
