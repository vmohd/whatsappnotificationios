//
//  AppDelegate.swift
//  WFamily
//
//  Created by Bullet Apps on 17/06/19.
//  Copyright © 2019 Bullet Apps. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications
import FirebaseMessaging
import Alamofire

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,MessagingDelegate {

    var window: UIWindow?
       var timer: Timer?
     var sec = 21600
     var totalSecondsTracking = String()
     var totalSeconds = 0
    let defaultss = UserDefaults.standard
     let deviceId = UIDevice.current.identifierForVendor?.uuidString

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        defaultss.set(deviceId, forKey: "DeviceId")
        // Override point for customization after application launch.
        FirebaseApp.configure()
        
//        FBAdSettings.addTestDevice(FBAdSettings.testDeviceHash())
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
       Messaging.messaging().delegate = self
       
       
        
        return true
    }
    
    // MARK: Firebase messeging Delegates
    // The callback to handle data message received via FCM for devices running iOS 10 or above.
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        defaultss.set(fcmToken, forKey: "FirebaseToken")
        print("Firebase token: \(fcmToken)")
        fcmTokenStr = fcmToken
         self.createUserOnWhatsAppStatusServer()
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
         defaultss.set(fcmToken, forKey: "FirebaseToken")
        print("Firebase registration token: \(fcmToken)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("i am not available in simulator \(error)")
    }
    
    
    //MARK: User notification Delegates
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        //foreground case
        
        print(notification.request.content.title)
        print(notification.request.content.body)
        
        completionHandler(.alert)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        //Background case
        
        print(response.notification.request.content.title)
        print(response.notification.request.content.body)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        if UserDefaults.standard.bool(forKey: kNumberTrack) {
            let currentTime = Utility.setCurrentTime_Date()
            UserDefaults.standard.set(currentTime, forKey: kCurrentTrackDateTime)
            
            UserDefaults.standard.set(totalSeconds, forKey: kTotalSeconds)
            
            self.totalSeconds = 0
            self.sec = 21600
        }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        /*
        if UserDefaults.standard.bool(forKey: kNumberTrack) {
            
            //to get current time when timer pause
            totalSecondsTracking = UserDefaults.standard.string(forKey: kCurrentTrackDateTime) ?? ""
            print(totalSecondsTracking)
            
            //to get total minutes of tracking
            let lastTotalSecondsTracking = UserDefaults.standard.integer(forKey: kTotalSeconds)
            print(lastTotalSecondsTracking)
            
            //to get time difference between two dates
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss a"
            //dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.calendar = Calendar.current
            dateFormatter.timeZone = TimeZone.current
            
            let nowTime = dateFormatter.string(from: Date())
            let current = dateFormatter.date(from: nowTime)
            let lastTrackTime = dateFormatter.date(from: totalSecondsTracking) ?? Date()
            print(lastTrackTime)
            
            let timeInterval = current?.timeIntervalSince(lastTrackTime)
            print(timeInterval ?? 0)
            
            totalSeconds = Int(lastTotalSecondsTracking) + Int((timeInterval ?? 0))
            print(totalSeconds)
            
            let totalMinute = Int((totalSeconds) / 60) % 60
            print(totalMinute)
            
            if totalMinute >= 480 {
                //pause the timer and disable the track button
                UserDefaults.standard.set(true, forKey: kTrialExpired)
                
                if let disable = disableTimerWithTrackBtn {
                    disable(totalSeconds)
                }
                
            }else {
                //start timer with addition of last track-time
                
                if let resume = resumeTimerWithLastTrackTime {
                    resume(totalSeconds)
                }
                
            }
            
        }
        */
       
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        if UserDefaults.standard.bool(forKey: kNumberTrack) {
            
            //to get current time when timer pause
            totalSecondsTracking = UserDefaults.standard.string(forKey: kCurrentTrackDateTime) ?? ""
            //print(totalSecondsTracking)
            
            //to get total minutes of tracking
            let lastTotalSecondsTracking = UserDefaults.standard.integer(forKey: kTotalSeconds)
            //print(lastTotalSecondsTracking)
            
            //to get time difference between two dates
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.locale = Locale(identifier: "fr_FR")
            //dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
            
            let nowTime = dateFormatter.string(from: Date())
            //print(nowTime)
            
            let current = dateFormatter.date(from: nowTime)
            //print(current ?? Date())
            
            let lastTrackTime = dateFormatter.date(from: totalSecondsTracking) ?? Date()
            //print(lastTrackTime)
            
            let timeInterval = current?.timeIntervalSince(lastTrackTime)
            //print(timeInterval ?? 0)
            
            totalSeconds = Int(lastTotalSecondsTracking) + Int((timeInterval ?? 0))
            //print(totalSeconds)
            
            let totalMinute = Int((totalSeconds) / 60)
            //print(totalMinute)
            
            if totalMinute >= 360 {
                //pause the timer and disable the track button
                UserDefaults.standard.set(true, forKey: kTrialExpired)
                
                if let disable = disableTimerWithTrackBtn {
                    //disable(totalSeconds)
                    disable()
                }
                
            }else {
                //start timer with addition of last track-time
                
                if let resume = resumeTimerWithLastTrackTime {
                    resume(totalSeconds)
                }
                
            }
            
        }
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
       
}

    
    func createUserOnWhatsAppStatusServer() {
             let token = defaultss.string(forKey: "FirebaseToken")
            let dvid = defaultss.string(forKey: "DeviceId")
            let parameters = ["device_id"   : dvid,
                              "token"       : token!,
                              "device_type" : "2",
                              "key"         : "AAAAQWqvdcc:APA91bFCSu7j0aasCamxBjIRLj3cJZIVXC48y6fFd5Lpp3MtcPDHradODP3jX5NxpjJMCVNQXnVprQrKmHI9sT92AfTB8saz-eRwnK1xT2Gl8E-qoBLzfJwaJ_Z0sE9PjGeSFU2YzEms"
                              ] as! [String:String]
            print(parameters)
            
            Alamofire.upload(multipartFormData: { (multiData) in
                
                for (key,value) in parameters {
                    multiData.append((value.data(using: String.Encoding.utf8)!), withName: key)
                }
                
            }, to: kCreateUser, method: .post, headers: kHeader) { (result) in
                
                switch result {
                case .success(let upload,_,_):
                    
                    upload.responseJSON(completionHandler: { (response) in
                        print(response.result.value ?? "no response")
                        print(response.response?.statusCode ?? 00)
                        
                        let dict = response.result.value as? [String:Any]
                        print(dict?["message"] ?? "message")
                        print(dict?["status"] ?? "status")

                        let result = dict?["result"] as? [[String:Any]]
                        let userid = result?[0]["id"] ?? 0
                         print("\(userid)")
                        self.defaultss.set(userid, forKey: "GetUserID")
    //                    self.subscribeUser(userID: "\(dict?["user_id"] ?? 0)")
                    })
                    
                case .failure(let encodingError):
                    print(encodingError)
                }
                
            }
            
        }
}

