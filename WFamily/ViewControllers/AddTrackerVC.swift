//
//  AddTrackerVC.swift
//  WFamily
//
//  Created by Bullet Apps on 17/06/19.
//  Copyright © 2019 Bullet Apps. All rights reserved.
//

import UIKit
import Alamofire

var noOfItems : Int?
var monthName : String?
var TrackData : [Double]?

class AddTrackerVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var addNumberFirstBtn: UIButton?
    @IBOutlet weak var noNumberTrackView: UIView?
    @IBOutlet weak var numbersTrackView: UIView?
    @IBOutlet weak var deleteHistoryBtn: UIButton?
    @IBOutlet weak var nowBtn: UIButton?
    @IBOutlet weak var dailyBtn: UIButton?
    @IBOutlet weak var weeklyBtn: UIButton?
    
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var btnVWHeightConstraint: NSLayoutConstraint?
    
    var arOnline:NSMutableArray = []
    var arOffline:NSMutableArray = []
    var arDailyOnline:NSMutableArray = []
    var arDailyOffline:NSMutableArray = []
    var calculateTime:NSMutableArray = []
    var calculateDailyTime:NSMutableArray = []
    var calculateForGraph = [Double]()
    var label:UILabel?
    @IBOutlet weak var graphView: UIView?
    var examples: Examples!
    var statusValue = ""
    var currentDate = ""
    var chartGraphView: ScrollableGraphView!
    var currentGraphType = GraphType.multiOne
    let mobileno = "9519500245"
    var deleteApiStatus = 0
    let track = UserDefaults.standard.dictionary(forKey: kTrackDict)
    var resultcount = 0
    override func viewDidLoad() {
        super.viewDidLoad()
         self.setUpUIElements()
//

    }
    func graphShow(){
        TrackData = self.calculateForGraph
       // TrackData = [5.0,25.0,40.0,15.0,30.0,50.0,35.0,20.0,45.0,10.0]
        noOfItems = TrackData?.count
        print("No. of item = \(noOfItems as! Int)")
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL"
        let nameOfMonth = dateFormatter.string(from: now)
        print(nameOfMonth)
        monthName = nameOfMonth
        examples = Examples()
        chartGraphView = examples.createMultiPlotGraphTwo(CGRect.init(x: (self.graphView?.frame.origin.x)!, y: (self.graphView?.frame.origin.y)! + 50, width: (self.graphView?.frame.size.width)!, height: 150))
        chartGraphView.tag = 1
        print(chartGraphView.frame.size.height)
        graphView?.insertSubview(chartGraphView, at: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        for v in graphView!.subviews{
               if v.tag == 1{
                   v.removeFromSuperview()
               }
           }
        let size = Utility.getIphoneDevice(vc: self)
        if size == iPhoneDeviceEnum.iPhone_5_5s_5c_SE.rawValue {
            addNumberFirstBtn?.titleLabel?.font = UIFont.init(name: usedFontName, size: 17.0)
            nowBtn?.titleLabel?.font = UIFont.init(name: usedFontName, size: 15.0)
            dailyBtn?.titleLabel?.font = UIFont.init(name: usedFontName, size: 15.0)
            weeklyBtn?.titleLabel?.font = UIFont.init(name: usedFontName, size: 15.0)
        }
        
        if UserDefaults.standard.bool(forKey: kNumberTrack) {
            let now = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            self.currentDate = dateFormatter.string(from: now)
            self.getReportsApiCall()
           
        }else {
            btnVWHeightConstraint?.constant = 0
            noNumberTrackView?.isHidden = false
            numbersTrackView?.isHidden = true
            addNumberFirstBtn?.setTitle("", for: .normal)
        }
        
    }
    
    //MARK: UITableView DataSource & Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView.tag{
        case 0:
            return 1
        case 1:
            return arOnline.count
        case 2:
            return arDailyOnline.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let size = Utility.getIphoneDevice(vc: self)
        switch tableView.tag {
        case 0:
            let nowCell = tableView.dequeueReusableCell(withIdentifier: "nowCell", for: indexPath)
            let image = nowCell.viewWithTag(10) as! UIImageView
            let label = nowCell.viewWithTag(20) as! UILabel
            let backBtn = nowCell.viewWithTag(30) as! UIButton
            if self.statusValue != ""{
                backBtn.layer.cornerRadius = (backBtn.frame.size.height)/4
                backBtn.layer.borderWidth = 0.2
                if size == iPhoneDeviceEnum.iPhone_5_5s_5c_SE.rawValue {
                    label.font = UIFont.init(name: usedFontName, size: 18.0)
                }
                image.layer.cornerRadius = image.frame.size.height/2
                print(self.statusValue)
                if self.statusValue == "Online"{
                    image.backgroundColor = UIColor.green
                    label.text = "Online"
                }else if self.statusValue == "Offline"{
                    image.backgroundColor = UIColor.red
                    label.text = "Offline"
                }
            }else{
                image.backgroundColor = nil
                label.text = ""
            }
            
            return nowCell
        case 1:
            let historyCell = tableView.dequeueReusableCell(withIdentifier: "historyCell", for: indexPath)
            
            let onlineImgView = historyCell.viewWithTag(10) as! UIImageView
            let onlineTimeLbl = historyCell.viewWithTag(20) as! UILabel
            let onlineDateLbl = historyCell.viewWithTag(30) as! UILabel
            let middleLbl = historyCell.viewWithTag(40) as! UILabel
            let offlineImgView = historyCell.viewWithTag(50) as! UIImageView
            let offlineTimeLbl = historyCell.viewWithTag(60) as! UILabel
            let offlineDateLbl = historyCell.viewWithTag(70) as! UILabel
            
            
            if size == iPhoneDeviceEnum.iPhone_5_5s_5c_SE.rawValue {
                onlineTimeLbl.font = UIFont.init(name: usedFontName, size: 16.0)
                onlineDateLbl.font = UIFont.init(name: usedFontName, size: 9.0)
                middleLbl.font = UIFont.init(name: usedFontName, size: 14.0)
                offlineTimeLbl.font = UIFont.init(name: usedFontName, size: 16.0)
                offlineDateLbl.font = UIFont.init(name: usedFontName, size: 9.0)
            }
            onlineImgView.layer.cornerRadius = onlineImgView.frame.size.height/2
            offlineImgView.layer.cornerRadius = offlineImgView.frame.size.height/2
            
            label = middleLbl

            let ontime=arOnline[indexPath.row] as! Double
            if ontime != 0
            {
                let date = Date(timeIntervalSince1970: TimeInterval(ontime))
                let ondt = self.convertUTCToLocalForDate(timeString: date)
                onlineDateLbl.text = ondt
                let cnvt = self.convertUTCToLocal(timeString: date)
                onlineTimeLbl.text = cnvt
            }
            else
            {
                onlineTimeLbl.text = "00:00     "
                onlineDateLbl.text = "00 00 0000     "
            }
            
            let offtime=arOffline[indexPath.row] as! Double
            if offtime != 0
            {
                let date = Date(timeIntervalSince1970: TimeInterval(offtime))
                let offdt = self.convertUTCToLocalForDate(timeString: date)
                offlineDateLbl.text = offdt
                let cnvt = self.convertUTCToLocal(timeString: date)
                offlineTimeLbl.text = cnvt
            }
            else
            {
                offlineTimeLbl.text = "00:00     "
                offlineDateLbl.text = "00 0000 0000     "
            }
            
            middleLbl.text = self.calculateTime[indexPath.row] as? String
            
            return historyCell
            
        case 2:
            let dailyCell = tableView.dequeueReusableCell(withIdentifier: "dailyCell", for: indexPath)
            
            let onlineImgView = dailyCell.viewWithTag(10) as! UIImageView
            let onlineTimeLbl = dailyCell.viewWithTag(20) as! UILabel
            let onlineDateLbl = dailyCell.viewWithTag(30) as! UILabel
            let middleLbl = dailyCell.viewWithTag(40) as! UILabel
            let offlineImgView = dailyCell.viewWithTag(50) as! UIImageView
            let offlineTimeLbl = dailyCell.viewWithTag(60) as! UILabel
            let offlineDateLbl = dailyCell.viewWithTag(70) as! UILabel
            
            
            if size == iPhoneDeviceEnum.iPhone_5_5s_5c_SE.rawValue {
                onlineTimeLbl.font = UIFont.init(name: usedFontName, size: 16.0)
                onlineDateLbl.font = UIFont.init(name: usedFontName, size: 9.0)
                middleLbl.font = UIFont.init(name: usedFontName, size: 14.0)
                offlineTimeLbl.font = UIFont.init(name: usedFontName, size: 16.0)
                offlineDateLbl.font = UIFont.init(name: usedFontName, size: 9.0)
            }
            onlineImgView.layer.cornerRadius = onlineImgView.frame.size.height/2
            offlineImgView.layer.cornerRadius = offlineImgView.frame.size.height/2
            
             label = middleLbl
            let ontime=arDailyOnline[indexPath.row] as! Double
            if ontime != 0
            {
                let date = Date(timeIntervalSince1970: TimeInterval(ontime))
                let ondt = self.convertUTCToLocalForDate(timeString: date)
                onlineDateLbl.text = ondt
                let cnvt = self.convertUTCToLocal(timeString: date)
                onlineTimeLbl.text = cnvt
            }
            else
            {
                onlineTimeLbl.text = "00:00     "
                onlineDateLbl.text = "00 00 0000     "
            }
            
            let offtime=arDailyOffline[indexPath.row] as! Double
            if offtime != 0
            {
                let date = Date(timeIntervalSince1970: TimeInterval(offtime))
                let offdt = self.convertUTCToLocalForDate(timeString: date)
                offlineDateLbl.text = offdt
                let cnvt = self.convertUTCToLocal(timeString: date)
                offlineTimeLbl.text = cnvt
            }
            else
            {
                offlineTimeLbl.text = "00:00     "
                offlineDateLbl.text = "00 0000 0000     "
            }
            
            middleLbl.text = self.calculateDailyTime[indexPath.row] as? String
            
            return dailyCell
        default:
            let nowCell = tableView.dequeueReusableCell(withIdentifier: "nowCell", for: indexPath)
            return nowCell
        }
    }
    
    
    //MARK: Custom Methods
    func setUpUIElements() {
        addNumberFirstBtn?.layer.cornerRadius = (addNumberFirstBtn?.frame.size.height)!/2
        addNumberFirstBtn?.layer.borderColor = #colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1)
        addNumberFirstBtn?.layer.borderWidth = 1.0
        
        deleteHistoryBtn?.layer.cornerRadius = (deleteHistoryBtn?.frame.size.height)!/2
        
        nowBtn?.layer.cornerRadius = (nowBtn?.frame.size.height)!/2
        nowBtn?.layer.borderColor = #colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1)
        nowBtn?.layer.borderWidth = 1.0
        
        dailyBtn?.layer.cornerRadius = (dailyBtn?.frame.size.height)!/2
        dailyBtn?.layer.borderColor = #colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1)
        dailyBtn?.layer.borderWidth = 1.0
        
        weeklyBtn?.layer.cornerRadius = (weeklyBtn?.frame.size.height)!/2
        weeklyBtn?.layer.borderColor = #colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1)
        weeklyBtn?.layer.borderWidth = 1.0
        
        
        
    }
    
    func deleteApiCall() {
        let uid = UserDefaults.standard.integer(forKey: "GetUserID")
        let parameters = [
            "user_id": String (describing: uid)]
        print(parameters)
        Alamofire.upload(multipartFormData: { (multiData) in
            
            for (key,value) in parameters {
                multiData.append((value.data(using: String.Encoding.utf8)!), withName: key)
            }
            
        }, to: kDelete, method: .post, headers: kHeader) { (result) in
            
            switch result {
            case .success(let upload,_,_):
                
                upload.responseJSON(completionHandler: { (response) in
                    print(response.result.value ?? "no response")
                    let dict = response.result.value as? [String:Any]
                    let status = dict?["status"] as! Int
                    
                    print("Status  = \(status)")
                    
                    self.deleteApiStatus  = status
                    
                })
            case .failure(let encodingError):
                print(encodingError)
            }
            
        }
        
    }
    
    
    func getReportsApiCall() {
        let uid = UserDefaults.standard.integer(forKey: "GetUserID")
        var number: String = ""
        if let trackdata = UserDefaults.standard.dictionary(forKey: kTrackDict) as? [String:String]{
            number = trackdata["trackCountryCode"]!+trackdata["trackNumber"]!
        }
        number.removeFirst()
        let parameters = [
            "user_id": String (describing: uid)  ,
            "mobile_number": number]
        print(parameters)
        
        //let manager = Alamofire.SessionManager.default
        //manager.session.configuration.timeoutIntervalForRequest = 120
        
        Alamofire.upload(multipartFormData: { (multiData) in
            
            for (key,value) in parameters {
                multiData.append((value.data(using: String.Encoding.utf8)!), withName: key)
            }
            
        }, to: kGetReports, method: .post, headers: kHeader) { (result) in
            
            switch result {
            case .success(let upload,_,_):
                
                upload.responseJSON(completionHandler: { (response) in
                    print(response.result.value ?? "no response")
                    let dict = response.result.value as? [String:Any]
                    let result = dict?["result"] as? [[String:Any]]
                    self.arOnline = []
                    self.arOffline = []
                    self.arDailyOffline = []
                    self.arDailyOnline = []
                    self.calculateTime = []
                    self.calculateDailyTime = []
                    self.calculateForGraph = []
                    self.resultcount = result?.count ?? 0
                    if self.resultcount != 0{
                        self.addNumberFirstBtn?.isHidden = false
                        self.btnVWHeightConstraint?.constant = 60
                        self.noNumberTrackView?.isHidden = true
                        self.numbersTrackView?.isHidden = false
                        
                        if let trackData = UserDefaults.standard.dictionary(forKey: kTrackDict) as? [String:String] {
                            let title = trackData["trackName"]?.uppercased(with: nil)
                            self.addNumberFirstBtn?.setTitle(title, for: .normal)
                        }
                        var count = 0
                        for i in result!{
                            self.statusValue = result?[0]["status"] as! String
                            let created = i["created"] as! String
                            let status = i["status"] as! String
                            let lastseen = i["last_seen"] as! String
                            let s = lastseen.split(separator: ".")[0]
                            if status == "Online"{
                                self.arOnline.add(Double(s)!)
                                if(count==0)
                                {
                                    self.arOffline.add(0)
                                }
                                
                            }else if status == "Offline"{
                                self.arOffline.add(Double(s)!)
                                if count==result!.count-1
                                {
                                    self.arOnline.add(0)
                                }
                            }
                            
                            if self.currentDate == created{
                                if status == "Online"{
                                    self.arDailyOnline.add(Double(s)!)
                                    if(count==0)
                                    {
                                        self.arDailyOffline.add(0)
                                    }
                                    
                                }else if status == "Offline"{
                                    self.arDailyOffline.add(Double(s)!)
                                    if count==result!.count-1
                                    {
                                        self.arDailyOnline.add(0)
                                    }
                                }
                            }
                            
                            count+=1;
                            
                        }
                    }else{
                        self.addNumberFirstBtn?.isHidden = true
                        self.btnVWHeightConstraint?.constant = 0
                        self.noNumberTrackView?.isHidden = false
                        self.numbersTrackView?.isHidden = true
                    }
                    self.calc_secondsweekly()
                    self.calc_secondsdaily()
                    self.tableView?.reloadData()
                     self.graphShow()
                })
            case .failure(let encodingError):
                print(encodingError)
            }
            
        }
        
    }
    func calc_secondsdaily(){
        var c = 0
        for i in self.arDailyOnline{
            if self.arDailyOnline.count >= 0{
                let onlineVal = i as? Double
                if c < self.arDailyOffline.count{
                    let offlineVal = self.arDailyOffline[c] as? Double
                    
                    if onlineVal != 0 && offlineVal != 0
                    {
                        let onlineTime=TimeInterval(onlineVal!)
                        let offlineTime=TimeInterval(offlineVal!)
                        //                            print((offlineTime-onlineTime))
                        var dt = offlineTime-onlineTime
                        if dt < 60{
                            if Int(dt) == 1{
                                self.calculateDailyTime.add("\(Int(dt)) sec  ")
                            }else{
                                self.calculateDailyTime.add("\(Int(dt)) secs")
                            }
                        }else{
                            dt = ((dt)/60)
                            if Int(dt) == 1{
                                self.calculateDailyTime.add("\(Int(dt)) min  ")
                            }else{
                                self.calculateDailyTime.add("\(Int(dt)) mins")
                            }
                        }
                    }else if onlineVal != 0 && offlineVal == nil{
                        label!.text = "0 sec  "
                    }
                    else
                    {
                        self.calculateDailyTime.add("0 sec")
                    }
                    
                }
                c += 1
            }
        }
    }
    
    func calc_secondsweekly(){
        var c = 0
        for i in self.arOnline{
            if self.arOnline.count >= 0{
                let onlineVal = i as? Double
                if c < self.arOffline.count{
                    let offlineVal = self.arOffline[c] as? Double
                    
                    if onlineVal != 0 && offlineVal != 0
                    {
                        let onlineTime=TimeInterval(onlineVal!)
                        let offlineTime=TimeInterval(offlineVal!)
                        //                            print((offlineTime-onlineTime))
                        var dt = offlineTime-onlineTime
                        if dt < 60{
                            if Int(dt) == 1{
                                self.calculateTime.add("\(Int(dt)) sec  ")
                                self.calculateForGraph.append(Double(dt))
                            }else{
                                self.calculateTime.add("\(Int(dt)) secs")
                                 self.calculateForGraph.append(Double(dt))
                            }
                        }else{
                            dt = ((dt)/60)
                            if Int(dt) == 1{
                                 self.calculateForGraph.append(Double(dt))
                                self.calculateTime.add("\(Int(dt)) min  ")
                            }else{
                                 self.calculateForGraph.append(Double(dt))
                                self.calculateTime.add("\(Int(dt)) mins")
                            }
                        }
                    }else if onlineVal != 0 && offlineVal == nil{
                        label!.text = "0 sec  "
                    }
                    else
                    {
                        self.calculateForGraph.append(Double(0))
                        self.calculateTime.add("0 sec")
                    }
                    
                }
                c += 1
            }
        }
    }
    
    func convertUTCToLocalss(timeString: Date) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "s"
        
        dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
        let timeUTC = timeString
        
        if timeUTC != nil {
            dateFormatter.timeZone = NSTimeZone.local
            let millis=timeUTC.timeIntervalSinceNow
            let localTime = ("\(millis/60)");
            return localTime
        }
        
        return nil
    }
    func convertUTCToLocalForDate(timeString: Date) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM y"
        dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
        let timeUTC = timeString
        
        if timeUTC != nil {
            dateFormatter.timeZone = NSTimeZone.local
            
            let localTime = dateFormatter.string(from: timeUTC)
            return localTime
        }
        
        return nil
    }
    
    
    func convertUTCToLocal(timeString: Date) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        
        dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
        let timeUTC = timeString
        
        if timeUTC != nil {
            dateFormatter.timeZone = NSTimeZone.local
            
            let localTime = dateFormatter.string(from: timeUTC)
            return localTime
        }
        
        return nil
    }
    
    
    //MARK: IBActions
    @IBAction func addNumberFirstButtonClicked(_ sender: UIButton) {
        
        /*
         sender.isSelected = !sender.isSelected
         
         if sender.isSelected {
         sender.backgroundColor = #colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1)
         sender.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
         
         if let scroll = scrollPage {
         scroll(0)
         }
         }else {
         sender.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
         sender.setTitleColor(#colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1), for: .normal)
         }
         */
    }
    
    @IBAction func deleteButtonClicked(_ sender: UIButton) {
        self.deleteApiCall()
        if self.deleteApiStatus == 1{
            btnVWHeightConstraint?.constant = 0
            noNumberTrackView?.isHidden = false
            numbersTrackView?.isHidden = true
            addNumberFirstBtn?.setTitle("", for: .normal)
        }
    }
    
    @IBAction func nowButtonClicked(_ sender: UIButton) {
        //        self.nowDataShow.isHidden = false
        //        self.historyDataShow.isHidden = true
        //sender.isSelected = !sender.isSelected
        tableView?.tag = 0
        self.tableView?.reloadData()
        
        sender.backgroundColor = #colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1)
        sender.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        
        dailyBtn?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        dailyBtn?.setTitleColor(#colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1), for: .normal)
        
        weeklyBtn?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        weeklyBtn?.setTitleColor(#colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1), for: .normal)
    }
    
    @IBAction func dailyButtonClicked(_ sender: UIButton) {
        
        //sender.isSelected = !sender.isSelected
        //        self.nowDataShow.isHidden = true
        //               self.historyDataShow.isHidden = false
        tableView?.tag = 2
        self.tableView?.reloadData()
        sender.backgroundColor = #colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1)
        sender.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        
        nowBtn?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        nowBtn?.setTitleColor(#colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1), for: .normal)
        
        weeklyBtn?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        weeklyBtn?.setTitleColor(#colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1), for: .normal)
    }
    
    @IBAction func weeklyButtonClicked(_ sender: UIButton) {
        
        //sender.isSelected = !sender.isSelected
        //        self.nowDataShow.isHidden = true
        //               self.historyDataShow.isHidden = false
        tableView?.tag = 1
        self.tableView?.reloadData()
        sender.backgroundColor = #colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1)
        sender.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        
        nowBtn?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        nowBtn?.setTitleColor(#colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1), for: .normal)
        
        dailyBtn?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        dailyBtn?.setTitleColor(#colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1), for: .normal)
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    
}

extension Dictionary {
    func percentEscaped() -> String {
        return map { (key, value) in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }
        .joined(separator: "&")
    }
}

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="
        
        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}
