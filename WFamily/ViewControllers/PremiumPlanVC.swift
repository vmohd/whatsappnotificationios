//
//  PremiumPlanVC.swift
//  WFamily
//
//  Created by Bullet Apps on 19/06/19.
//  Copyright © 2019 Bullet Apps. All rights reserved.
//

import UIKit

class PremiumPlanVC: UIViewController,premiumPageViewControllerDelegate {
        
    @IBOutlet weak var singleNum_btn: UIButton?
    @IBOutlet weak var twoNum_btn: UIButton?
    @IBOutlet weak var singleNum_lbl: UILabel?
    @IBOutlet weak var twokNum_lbl: UILabel?
    
    //MARK: SetUp PageViewController work here
    var premiumViewController: premiumPageViewController? {
        didSet {
            premiumViewController?.premiumDelegate = self
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        singleNum_btn?.addTarget(self, action: #selector(buttonClicked(sender:)), for: .touchUpInside)
        twoNum_btn?.addTarget(self, action: #selector(buttonClicked(sender:)), for: .touchUpInside)
    }
    
    @objc func buttonClicked(sender:UIButton) {
        switch sender.tag {
        case 0:
            premiumViewController?.scrollToViewController(index: 0)
        default:
            premiumViewController?.scrollToViewController(index: 1)
        }
    }

    //MARK: IBActions
    @IBAction func backButtonClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: PremiumPage viewController Delegates
    func PageViewController(_ PageViewController: premiumPageViewController, didUpdatePageCount count: Int) {
        print("Count : \(count)")
    }
    
    func PageViewController(_ PageViewController: premiumPageViewController, didUpdatePageIndex index: Int) {
        switch index {
        case 0:
            singleNum_lbl?.isHidden = false
            twokNum_lbl?.isHidden = true
        default:
            singleNum_lbl?.isHidden = true
            twokNum_lbl?.isHidden = false
        }
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if let premiumPageViewController = segue.destination as? premiumPageViewController {
            self.premiumViewController = premiumPageViewController
        }
    }

}
