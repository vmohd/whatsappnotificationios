//
//  SingleNumberVC.swift
//  WFamily
//
//  Created by Bullet Apps on 19/06/19.
//  Copyright © 2019 Bullet Apps. All rights reserved.
//

import UIKit

class SingleNumberVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView?
    var planDuration : String?
    var planDetail : String?
    var planPurchase : Bool?
    
    let arrTitle = ["WEEKLY PREMIUM","MONTHLY PREMIUM","3 MONTH PREMIUM"]
    let arrPrice = ["500.00","1000.00","1500.00"]
    let arrPlan = ["per week","per month","per 3 month"]
    let arrPlanDetail = ["Track Choosen Number For A Week","Track Choosen Number For Full Month","Track Choosen Number For Three Months"]
    
    let arrTopColor = [#colorLiteral(red: 0.6383562684, green: 0.385876596, blue: 0.9951258302, alpha: 1),#colorLiteral(red: 0.6263700128, green: 0.8068521619, blue: 0.3991550803, alpha: 1),#colorLiteral(red: 0.8998566866, green: 0.6605263948, blue: 0.1089573577, alpha: 1)]
    let arrBottomColor = [#colorLiteral(red: 0.2947509289, green: 0.3940112591, blue: 0.9951260686, alpha: 1),#colorLiteral(red: 0.2355087698, green: 0.7400442958, blue: 0.5624403954, alpha: 1),#colorLiteral(red: 0.9288108945, green: 0.5563133955, blue: 0.0527921319, alpha: 1)]

    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    //MARK: Custom Methods
    func setGradientBackground(topColor:UIColor,bottomColor:UIColor,vw:UIView) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.locations = [0, 1]
        gradientLayer.frame = vw.bounds
        
        vw.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    @objc func getPlanButtonClicked(_ sender:UIButton) {
        
        UserDefaults.standard.set(true, forKey: kPremiumTrack)
        
        self.tableView?.beginUpdates()
        planPurchase = true

        switch sender.tag {
        case 0:
            planDuration = "FOR 1 WEEK"
            planDetail = "Track Single Number For Full Week"
        case 1:
            planDuration = "FOR 1 MONTH"
            planDetail = "Track Single Number For Full Month"
        default:
            planDuration = "FOR 3 MONTHS"
            planDetail = "Track Single Number For Three Months"
        }
        
        self.tableView?.endUpdates()
    }
    
    //MARK: IBActions
    @IBAction func backButtonClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: UITableView DataSource & Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitle.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if arrTitle.count > indexPath.row {
            let planCell = tableView.dequeueReusableCell(withIdentifier: "planCell", for: indexPath)
            
            let baseView = planCell.viewWithTag(10) ?? UIView()
            let titleLbl = planCell.viewWithTag(20) as! UILabel
            let priceLbl = planCell.viewWithTag(30) as! UILabel
            let planLbl = planCell.viewWithTag(40) as! UILabel
            let planDetailLbl = planCell.viewWithTag(50) as! UILabel
            let getPlanBtn = planCell.viewWithTag(60) as? UIButton
            let sideLbl = planCell.viewWithTag(90) as! UILabel
            
            sideLbl.roundCorners(corners: [.bottomRight], radius: 15.0)
            
            if indexPath.row == 2 {
                sideLbl.isHidden = false
            }else {
                sideLbl.isHidden = true
            }
            
            getPlanBtn?.tag = indexPath.row
            getPlanBtn?.layer.cornerRadius = (getPlanBtn?.frame.size.height)!/2
            getPlanBtn?.addTarget(self, action: #selector(getPlanButtonClicked(_:)), for: .touchUpInside)
            
            let size = Utility.getIphoneDevice(vc: self)
            if size == iPhoneDeviceEnum.iPhone_5_5s_5c_SE.rawValue {
                getPlanBtn?.titleLabel?.font = UIFont.init(name: usedFontName, size: 17.0)
            }
            
            // Shadow and Radius for Button
            getPlanBtn?.layer.shadowColor = UIColor.black.cgColor
            getPlanBtn?.layer.shadowOpacity = shadowOpacity
            getPlanBtn?.layer.shadowRadius = shadowRadius
            getPlanBtn?.layer.shadowOffset = CGSize.zero
            getPlanBtn?.layer.masksToBounds = false
            
            titleLbl.text = arrTitle[indexPath.row]
            priceLbl.text = arrPrice[indexPath.row]
            planLbl.text = arrPlan[indexPath.row]
            planDetailLbl.text = arrPlanDetail[indexPath.row]
            
            baseView.layer.cornerRadius = cornerRadius
            self.setGradientBackground(topColor: arrTopColor[indexPath.row], bottomColor: arrBottomColor[indexPath.row], vw: baseView)
            
            return planCell
        }else {
            let planCell = tableView.dequeueReusableCell(withIdentifier: "planActivationCell", for: indexPath)
            
            let baseView = planCell.viewWithTag(10) ?? UIView()
            let sideLbl = planCell.viewWithTag(20) as! UILabel
            let planDurationLbl = planCell.viewWithTag(30) as! UILabel
            let planDetailLbl = planCell.viewWithTag(40) as! UILabel
            
            baseView.layer.cornerRadius = cornerRadius
            sideLbl.roundCorners(corners: [.bottomRight], radius: 15.0)
            planDurationLbl.text = planDuration ?? ""
            planDetailLbl.text = planDetail ?? ""
            
            return planCell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 0,1,2:
            return planPurchase == true ? 0 : UITableView.automaticDimension
        default:
            return planPurchase == true ? UITableView.automaticDimension : 0
        }
    }

    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }

}
