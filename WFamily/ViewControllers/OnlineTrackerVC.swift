//
//  ViewController.swift
//  WFamily
//
//  Created by Bullet Apps on 17/06/19.
//  Copyright © 2019 Bullet Apps. All rights reserved.
//

import UIKit

var scrollPage:((Int)->(Void))?

class OnlineTrackerVC: UIViewController,pageViewControllerDelegate {
   
    @IBOutlet weak var addNum_btn: UIButton?
    @IBOutlet weak var trackNum_btn: UIButton?
    @IBOutlet weak var addNum_lbl: UILabel?
    @IBOutlet weak var trackNum_lbl: UILabel?
    
    //MARK: SetUp PageViewController work here
    var pageViewController: PageViewController? {
        didSet {
            pageViewController?.tutorialDelegate = self
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        addNum_btn?.addTarget(self, action: #selector(buttonClicked(sender:)), for: .touchUpInside)
        trackNum_btn?.addTarget(self, action: #selector(buttonClicked(sender:)), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        scrollPage = { (index) in
            self.pageViewController?.scrollToViewController(index: index)
        }
    }
    
    @objc func buttonClicked(sender:UIButton) {
        switch sender.tag {
        case 0:
            pageViewController?.scrollToViewController(index: 0)
        default:
            pageViewController?.scrollToViewController(index: 1)
        }
    }
    
    //MARK: IBActions
    @IBAction func settingButtonClicked(_ sender: UIButton) {
        let vc = Utility.loadViewControllerFromSettingStoryBoard(identifier: "SettingVC") as! SettingVC
        self.navigationController?.pushViewController(vc, animated: true)
    }

    //MARK: pageViewController Delegates
    func PageViewController(_ PageViewController: PageViewController, didUpdatePageCount count: Int) {
        print("Count : \(count)")
    }
    
    func PageViewController(_ PageViewController: PageViewController, didUpdatePageIndex index: Int) {
        switch index {
        case 0:
            addNum_lbl?.isHidden = false
            trackNum_lbl?.isHidden = true
        default:
            addNum_lbl?.isHidden = true
            trackNum_lbl?.isHidden = false
        }
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if let tutorialPageViewController = segue.destination as? PageViewController {
            self.pageViewController = tutorialPageViewController
        }
    }

}

