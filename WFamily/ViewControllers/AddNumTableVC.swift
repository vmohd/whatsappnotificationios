//
//  AddNumTableVC.swift
//  WFamily
//
//  Created by Bullet Apps on 18/06/19.
//  Copyright © 2019 Bullet Apps. All rights reserved.
//

import UIKit
import Alamofire
import ContactsUI

struct Response: Codable {
    let result: String
    let suscriber: String
}


let apdel = UIApplication.shared.delegate as! AppDelegate

var disableTimerWithTrackBtn : (() -> Void)?
var resumeTimerWithLastTrackTime : ((Int) -> Void)?

class AddNumTableVC: UITableViewController,MICountryPickerDelegate,CNContactPickerDelegate {
    
    @IBOutlet weak var trackView1: UIView?
    @IBOutlet weak var countryImgView1: UIImageView?
    @IBOutlet weak var countryCodeBtn1: UIButton?
    @IBOutlet weak var countryCodeLbl1: UILabel?
    @IBOutlet weak var txtFieldName1: UITextField!
    @IBOutlet weak var txtFieldNumber1: UITextField?
    @IBOutlet weak var GetPremiumBtn1: UIButton?
    @IBOutlet weak var GetPremiumView1: UIView?
    @IBOutlet weak var tryFreeLbl1: UILabel?
    @IBOutlet weak var trackLbl1: UILabel?
    @IBOutlet weak var contactListBtn: UIButton?
    @IBOutlet weak var dialCodeBtn: UIButton?
    @IBOutlet weak var timerImgView: UIImageView?
    @IBOutlet weak var trackView3: UIView?
    @IBOutlet weak var GetPremiumBtn3: UIButton?
    
    @IBOutlet weak var unlimitedTrackingBtn: UIButton?
    @IBOutlet weak var detailedStatisticsBtn: UIButton?
    @IBOutlet weak var fasterNotificationBtn: UIButton?
    @IBOutlet weak var supportBtn: UIButton?
    
    let bundle = "assets.bundle/"
    var dialCode1,dialCode2 : String?
    var countryCode1,countryCode2 : String?
    var pickerTag: Int?
    let defaultss = UserDefaults.standard
    var ipAddress : String?
//    var bannerAdView:FBAdView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUiElements()
//        createUserOnWhatsAppStatusServer()
        
//        loadFacebookBanner()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let size = Utility.getIphoneDevice(vc: self)
        if size == iPhoneDeviceEnum.iPhone_5_5s_5c_SE.rawValue {
            GetPremiumBtn1?.titleLabel?.font = UIFont.init(name: usedFontName, size: 20.0)
            
            unlimitedTrackingBtn?.titleLabel?.font = UIFont.init(name: usedFontName, size: 10.0)
            detailedStatisticsBtn?.titleLabel?.font = UIFont.init(name: usedFontName, size: 10.0)
            fasterNotificationBtn?.titleLabel?.font = UIFont.init(name: usedFontName, size: 10.0)
            supportBtn?.titleLabel?.font = UIFont.init(name: usedFontName, size: 10.0)
        }
        
        if pickerTag == 1 {
            countryCodeLbl1?.text = countryCode1 ?? "IN"
            countryCodeBtn1?.setTitle(dialCode1 ?? "+91", for: .normal)
            countryImgView1?.image = UIImage(named: bundle + (countryCode1?.lowercased() ?? "in") + ".png", in: Bundle(for: MICountryPicker.self), compatibleWith: nil)
        }
        
        self.hideKeyboardWhenTappedAround()
   if UserDefaults.standard.bool(forKey: kNumberTrack) {
            
            if UserDefaults.standard.bool(forKey: kTrialExpired) {
                
                DispatchQueue.main.async {
                    self.GetPremiumBtn1?.backgroundColor = .gray
                    self.GetPremiumBtn1?.setTitle("Trial Expired", for: .normal)
                }
                
                self.tryFreeLbl1?.isHidden = true
                self.trackLbl1?.isHidden = true
                self.timerImgView?.isHidden = true
                
                self.txtFieldName1.isUserInteractionEnabled = false
                self.txtFieldNumber1?.isUserInteractionEnabled = false
                self.contactListBtn?.isUserInteractionEnabled = false
                self.dialCodeBtn?.isUserInteractionEnabled = false
                
            }else {
                
                DispatchQueue.main.async {
                    self.GetPremiumBtn1?.backgroundColor = #colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1)
                }
                
                self.tryFreeLbl1?.text = "Stop Tracking"
                self.trackLbl1?.text = "00:00:00"
                
                self.txtFieldName1.isUserInteractionEnabled = false
                self.txtFieldNumber1?.isUserInteractionEnabled = false
                self.contactListBtn?.isUserInteractionEnabled = false
                self.dialCodeBtn?.isUserInteractionEnabled = false
                
                if let trackData = UserDefaults.standard.dictionary(forKey: kTrackDict) as? [String:String] {
                    txtFieldName1.text = trackData["trackName"]
                    txtFieldNumber1?.text = trackData["trackNumber"]
                    countryCodeBtn1?.setTitle(trackData["trackCountryCode"], for: .normal)
                    countryCodeLbl1?.text = trackData["trackCountry"]
                }
            }
        }else {
            
            DispatchQueue.main.async {
                self.GetPremiumBtn1?.backgroundColor = #colorLiteral(red: 0.8386928439, green: 0.1190194711, blue: 0.4354643822, alpha: 1)
            }
            
            self.tryFreeLbl1?.text = "Start Tracking"
            let timeleft = UserDefaults.standard.integer(forKey: kTotalSeconds)
            let (h,m,s) = timeCalculation(time: timeleft)
             self.trackLbl1?.text = "\(h):\(m):\(s)"
            
            self.txtFieldName1.isUserInteractionEnabled = true
            self.txtFieldNumber1?.isUserInteractionEnabled = true
            self.contactListBtn?.isUserInteractionEnabled = true
            self.dialCodeBtn?.isUserInteractionEnabled = true
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        disableTimerWithTrackBtn = {
            
            DispatchQueue.main.async {
                self.GetPremiumBtn1?.backgroundColor = .gray
                self.GetPremiumBtn1?.setTitle("Trial Expired", for: .normal)
            }
            
            self.tryFreeLbl1?.isHidden = true
            self.trackLbl1?.isHidden = true
            self.timerImgView?.isHidden = true
            
            self.txtFieldName1.isUserInteractionEnabled = true
            self.txtFieldNumber1?.isUserInteractionEnabled = true
            self.contactListBtn?.isUserInteractionEnabled = true
            self.dialCodeBtn?.isUserInteractionEnabled = true
            
        }
        
        resumeTimerWithLastTrackTime = { (seconds) in
            
            apdel.sec = seconds
            apdel.timer?.invalidate()
            apdel.timer = nil
            
            apdel.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.startTimer), userInfo: nil, repeats: true)
        }
    }
    
    //MARK: Custom Methods
    func setUiElements() {
        // Shadow and Radius for Button
        GetPremiumView1?.layer.shadowColor = UIColor.black.cgColor
        GetPremiumView1?.layer.shadowOpacity = shadowOpacity
        GetPremiumView1?.layer.shadowRadius = shadowRadius
        GetPremiumView1?.layer.shadowOffset = CGSize.zero
        GetPremiumView1?.layer.masksToBounds = false
        
        DispatchQueue.main.async {
            self.trackView1?.layer.cornerRadius = cornerRadius
            self.trackView3?.layer.cornerRadius = cornerRadius
            
            self.GetPremiumView1?.layer.cornerRadius = cornerRadius
            self.GetPremiumBtn1?.layer.cornerRadius = cornerRadius
            self.GetPremiumBtn3?.layer.cornerRadius = cornerRadius
        }
        contactListBtn?.isHidden = true
        countryCodeLbl1?.text = countryCode1 ?? "IN"
        countryCodeBtn1?.setTitle(dialCode1 ?? "+91", for: .normal)
        countryImgView1?.image = UIImage(named: bundle + (countryCode1?.lowercased() ?? "in") + ".png", in: Bundle(for: MICountryPicker.self), compatibleWith: nil)
    }
    
    @objc func startTimer() {
        apdel.sec -= 1
        print("apdel.sec \(apdel.sec)")
        
        let (h,m,s) = timeCalculation(time: apdel.sec)
        
        let minut = apdel.sec / 60
        print(minut)
        
        trackLbl1?.text = "\(h):\(m):\(s)"
        
        guard minut >= 360 else {
            return
        }
        
        if let disable = disableTimerWithTrackBtn {
            
            //pause the timer and disable the track button
            UserDefaults.standard.set(true, forKey: kTrialExpired)
            
            apdel.sec = 21600
            apdel.totalSeconds = 0
            apdel.timer?.invalidate()
            
            disable()
        }
        
    }
    
    func timeCalculation(time:Int) -> (String,String,String) {
        let hours = Int(apdel.sec) / 3600
        let minutes = Int(apdel.sec) / 60 % 60
        let seconds = Int(apdel.sec) % 60
        
        apdel.totalSeconds = (Int(apdel.sec) % 60) + (minutes * 60)
        print("apdel.totalSeconds \(apdel.totalSeconds)")
        
        return (String.init(format: "%02i",hours),String.init(format: "%02i",minutes),String.init(format: "%02i",seconds))
    }
    
    //MARK: IBActions
    @IBAction func btnCountryCodeClicked(_ sender: UIButton) {
        pickerTag = sender.tag
        
        let picker = MICountryPicker { (name, code) -> () in
            //print(code)
        }
        
        // Optional: To pick from custom countries list
        // picker.customCountriesCode = ["EG", "US", "AF", "AQ", "AX"]
        
        // delegate
        picker.delegate = self
        
        // Display calling codes
        // picker.showCallingCodes = true
        
        // or closure
        picker.didSelectCountryClosure = { name, code in
            // picker.navigationController?.popViewController(animated: true)
            picker.dismiss(animated: true, completion: nil)
        }
        
        // navigationController?.pushViewController(picker, animated: true)
        self.present(picker, animated: true, completion: nil)
    }
    
    @IBAction func getPremiumButtonClicked(_ sender: UIButton) {
        
        if GetPremiumBtn1?.titleLabel?.text == "Trial Expired" {
            
            let vc = Utility.loadViewControllerFromSettingStoryBoard(identifier: "SingleNumberVC") as! SingleNumberVC
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else {
            
            if !UserDefaults.standard.bool(forKey: kNumberTrack) {
                
                guard txtFieldName1?.text?.count ?? 0 > 0 else {
                    self.showaAlert(message: "Please Enter Name To Track!")
                    return
                }
                
                guard txtFieldNumber1?.text?.count ?? 0 > 0 else {
                    self.showaAlert(message: "Please Enter Number To Track!")
                    return
                }
                
                self.showAlert("Please Check & Confirm This Number.", message: "\(countryCodeBtn1?.titleLabel?.text ?? "") \(txtFieldNumber1?.text ?? "")", alertButtonTitles: ["Edit Number","Confirm"], alertButtonStyles: [.default,.default], vc: self) { (index) in
                    
                    if index == 0 {
                        self.txtFieldNumber1?.becomeFirstResponder()
                        self.dismiss(animated: true, completion: nil)
                    }else {
                        // Start Tracking here...
                        apdel.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.startTimer), userInfo: nil, repeats: true)
                        
                        self.GetPremiumBtn1?.backgroundColor = #colorLiteral(red: 0.07804057747, green: 0.1440337002, blue: 0.3141562343, alpha: 1)
                        self.tryFreeLbl1?.text = "Stop Tracking"
                        self.trackLbl1?.text = "06:00:00"
                        
                        self.txtFieldName1.isUserInteractionEnabled = false
                        self.txtFieldNumber1?.isUserInteractionEnabled = false
                        self.contactListBtn?.isUserInteractionEnabled = false
                        self.dialCodeBtn?.isUserInteractionEnabled = false
                        var countrycode = self.countryCodeBtn1?.titleLabel?.text
                        countrycode?.removeFirst()
                      
                        let TrackDict = ["id"               : self.defaultss.string(forKey: "DeviceId"),
                                         "trackName"        : self.txtFieldName1.text,
                                         "trackNumber"      : self.txtFieldNumber1?.text,
                                         "trackCountryCode" : self.countryCodeBtn1?.titleLabel?.text,
                                         "trackCountry"     : self.countryCodeLbl1?.text] as [String : Any]
                        
                        UserDefaults.standard.set(TrackDict, forKey: kTrackDict)
                        UserDefaults.standard.set(true, forKey: kNumberTrack)
                        let usid = UserDefaults.standard.integer(forKey: "GetUserID")
                        print("Suscriber userid: \(usid)")
                        self.subscribeUser(userID: "\(usid)")
                        
                    }
                }
                
            }else {
                
                apdel.timer?.invalidate()
                apdel.timer = nil
                
                self.GetPremiumBtn1?.backgroundColor = #colorLiteral(red: 0.8386928439, green: 0.1190194711, blue: 0.4354643822, alpha: 1)
                self.tryFreeLbl1?.text = "Start Tracking"
                self.trackLbl1?.text = "Track Any Number For 6 Hours"
                
                self.txtFieldName1.isUserInteractionEnabled = true
                self.txtFieldNumber1?.isUserInteractionEnabled = true
                self.contactListBtn?.isUserInteractionEnabled = true
                self.dialCodeBtn?.isUserInteractionEnabled = true
                
                UserDefaults.standard.removeSuite(named: kTrackDict)
                UserDefaults.standard.removeObject(forKey: kNumberTrack)
                
                apdel.timer?.invalidate()
                apdel.timer = nil
                apdel.totalSeconds = 0
                apdel.sec = 21600
            }
            
        }        
        
    }
    
    
    
    func subscribeUser(userID:String) {
        var name: String = ""
        var number: String = ""
        if let trackdata = UserDefaults.standard.dictionary(forKey: kTrackDict) as? [String:String]{
            name = trackdata["trackName"]!
            number = trackdata["trackCountryCode"]!+trackdata["trackNumber"]!
        }
        number.removeFirst()
        let parameters = ["user_id"       : userID,
                          "name"          : name,
                          "mobile_number" : number]
        print(parameters)
        
        //let manager = Alamofire.SessionManager.default
        //manager.session.configuration.timeoutIntervalForRequest = 120
        
        Alamofire.upload(multipartFormData: { (multiData) in
            
            for (key,value) in parameters {
                multiData.append((value.data(using: String.Encoding.utf8)!), withName: key)
            }
            
        }, to: kSubscribeUser, method: .post, headers: kHeader) { (result) in
            
            switch result {
            case .success(let upload,_,_):
                
                upload.responseJSON(completionHandler: { (response) in
                    print(response.result.value ?? "no response")
                    
                    let dict = response.result.value as? [String:Any]
                    self.ipAddress = (dict?["ip"] as! String)
                    print(self.ipAddress!)
                    
                    self.suscriberCheckPostApi()
                })
                
                
                
            case .failure(let encodingError):
                print(encodingError)
            }
            
        }
        
    }
    
    
    func suscriberCheckPostApi() {
        let ipadd = "http://" + self.ipAddress! + "/subscribeUser"
        var number: String = ""
               if let trackdata = UserDefaults.standard.dictionary(forKey: kTrackDict) as? [String:String]{
                   number = trackdata["trackCountryCode"]!+trackdata["trackNumber"]!
               }
        number.removeFirst()
        let parameters = ["mobile_number" : number]
       
        print(parameters)
        Alamofire.upload(multipartFormData: { (multiData) in
                   
                   for (key,value) in parameters {
                       multiData.append((value.data(using: String.Encoding.utf8)!), withName: key)
                   }
                   
               }, to: ipadd, method: .post, headers: kHeader) { (result) in
                   
                   switch result {
                   case .success(let upload,_,_):
                       
                       upload.responseString(completionHandler: { (response) in
                           print(response.result.value ?? "no response")
                          
                       })
                       
                       
                       
                   case .failure(let encodingError):
                       print(encodingError)
                   }
                   
               }
    }
    
    func updateTracking() {
     let uid = UserDefaults.standard.integer(forKey: "GetUserID")

         let parameters = ["user_id" : String (describing: uid)]
         print(parameters)
         
         //let manager = Alamofire.SessionManager.default
         //manager.session.configuration.timeoutIntervalForRequest = 120
         
         Alamofire.upload(multipartFormData: { (multiData) in
             
             for (key,value) in parameters {
                 multiData.append((value.data(using: String.Encoding.utf8)!), withName: key)
             }
             
         }, to: kUpdateTracking, method: .post, headers: kHeader) { (result) in
             
             switch result {
             case .success(let upload,_,_):
                 
                 upload.responseJSON(completionHandler: { (response) in
                     print(response.result.value ?? "no response")
                     
                     let dict = response.result.value as? [String:Any]
                    
                 })
             case .failure(let encodingError):
                 print(encodingError)
             }
             
         }
         
     }
    func changeTrackingStatus() {
      
        let status = "1"
        let uid = UserDefaults.standard.integer(forKey: "GetUserID")
         let parameters = [
            "user_id": String (describing: uid),
             "status" : status]
         print(parameters)
         Alamofire.upload(multipartFormData: { (multiData) in
             
             for (key,value) in parameters {
                 multiData.append((value.data(using: String.Encoding.utf8)!), withName: key)
             }
             
         }, to: kChangeTracking, method: .post, headers: kHeader) { (result) in
             
             switch result {
             case .success(let upload,_,_):
                 
                 upload.responseJSON(completionHandler: { (response) in
                     print(response.result.value ?? "no response")
                     let dict = response.result.value as? [String:Any]
                     let status = dict?["status"] as! Int
                 
                     
                 })
             case .failure(let encodingError):
                 print(encodingError)
             }
             
         }
         
     }
    @IBAction func getPremiumButton3Clicked(_ sender: UIButton) {
        let vc = Utility.loadViewControllerFromSettingStoryBoard(identifier: "SingleNumberVC") as! SingleNumberVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func contactListButtonClicked(_ sender: UIButton) {
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        contactPicker.displayedPropertyKeys =
            [CNContactGivenNameKey
                , CNContactPhoneNumbersKey]
        self.present(contactPicker, animated: true, completion: nil)
    }
    
    @IBAction func addMoreNumberButtonClicked(_ sender: UIButton) {
        
    }
    
    //MARK: ContactPicker Delegate
    func contactPicker(_ picker: CNContactPickerViewController,
                       didSelect contactProperty: CNContactProperty) {
        
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        // You can fetch selected name and number in the following way
        
        // user name
        let userName:String = contact.givenName
        print(userName)
        
        // user phone number
        let userPhoneNumbers:[CNLabeledValue<CNPhoneNumber>] = contact.phoneNumbers
        
        guard userPhoneNumbers.count > 0 else {
            return
        }
        
        let firstPhoneNumber:CNPhoneNumber = userPhoneNumbers[0].value
        
        // user phone number string
        let primaryPhoneNumberStr:String = firstPhoneNumber.stringValue
        print(primaryPhoneNumberStr)
        
        txtFieldName1.text = userName
        txtFieldNumber1?.text = primaryPhoneNumberStr
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        
    }
    
    //MARK: MICountryPicker Delegates
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String) {
        
    }
    
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        if pickerTag == 1 {
            self.dialCode1 = dialCode
            self.countryCode1 = code
        }else {
            self.dialCode2 = dialCode
            self.countryCode2 = code
        }
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        //return 2
        
        return UserDefaults.standard.bool(forKey: kPremiumTrack) ? 1 : 2
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //MARK: Web Service
    
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    
//    func loadFacebookBanner() {
//
//        bannerAdView = FBAdView(placementID: "382472159813100_382473499812966", adSize: kFBAdSizeHeight50Banner, rootViewController: self)
//        bannerAdView.frame = CGRect(x: 0, y: view.bounds.height - bannerAdView.frame.size.height, width: bannerAdView.frame.size.width, height: bannerAdView.frame.size.height)
//        bannerAdView.delegate = self
//        bannerAdView.isHidden = true
//        self.view.addSubview(bannerAdView)
//        bannerAdView.loadAd()
//
//    }
//
//    func adViewDidLoad(_ adView: FBAdView) {
//        bannerAdView.isHidden = false
//    }
//
//    func adView(_ adView: FBAdView, didFailWithError error: Error) {
//        print(error)
//    }
    
}
